provider "google" {
  credentials = file(pathexpand(var.credentials))
  region      = var.region
  project     = var.project
}

resource "google_storage_bucket" "bucket" {
  name     = var.bucket
  location = var.region
}

data "archive_file" "app" {
  type        = "zip"
  source_dir  = "${path.module}/javascript"
  output_path = "${path.module}/index.zip"
}

resource "google_storage_bucket_object" "archive" {
  name   = "index.zip"
  bucket = google_storage_bucket.bucket.name
  source = "./index.zip"
}

resource "google_cloudfunctions_function" "function" {
  name        = var.name
  description = "My function"
  runtime     = "nodejs10"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  entry_point           = "handler"
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
