# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "project" {
  description = "my project"
//  export TF_VAR_project=your project ID
}

variable "credentials" {
  description = "Path to credentials json for your service account"
//  export TF_VAR_credentials=/home/root/creds.json
}


variable "region" {
  description = "Region to deploy the function"
  type        = string
  default     = "us-central1"
}

variable "bucket" {
  description = "Bucket to store the function"
  type        = string
  default     = "super-test-bucket"
}

variable "name" {
  description = "The name of the function"
  type        = string
  default     = "hello-world-app"
}