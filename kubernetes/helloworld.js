const finalhandler = require('finalhandler')
const http = require('http')
const morgan = require('morgan')

const logger = morgan('combined')

http.createServer((req, res) => {
    const done = finalhandler(req, res)
    logger(req, res, (err) => {
    if (err) return done(err)
    res.setHeader('content-type', 'text/plain')
    res.end('hello, world!')
    })
}).listen(8080)