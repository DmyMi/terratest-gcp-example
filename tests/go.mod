module terratest-example

go 1.15

require (
	github.com/gruntwork-io/terratest v0.31.3
	github.com/stretchr/testify v1.4.0
)
